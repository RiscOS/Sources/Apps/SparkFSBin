# Makefile for !SparkFS
#

COMPONENT  = SparkFS
INSTTYPE   = app
CUSTOMINSTALLAPP = custom

include CApp

install_app:
	${MKDIR} ${INSTAPP}
	${CP} Resources ${INSTAPP} ${CPFLAGS}

# Dynamic dependencies:
